﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SelectedList : MonoBehaviour
{
    private Text txt;
    private void Awake() {
        txt = GetComponent<Text>();
        SelectionRect.Instance.OnSelectionChanged += OnSelectionChanged;
    }

    public void OnSelectionChanged()
    {
        System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
        stringBuilder.AppendLine("<b>Selected:</b>");
        foreach (Selectable entity in SelectionRect.Instance.selected)
        {
            stringBuilder.AppendLine(entity.GetComponent<UnitBase>().unitName);
        }
        txt.text = stringBuilder.ToString();
    }


}
