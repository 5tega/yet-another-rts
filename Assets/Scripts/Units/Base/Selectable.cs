﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectable : MonoBehaviour
{
    public delegate void SelectEvent();
    public SelectEvent OnSelect;
    public SelectEvent OnDeselect;
}
