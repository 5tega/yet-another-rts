﻿/*

Author: Stefan Stegic
Date: 23.4.2019.
Description: Base Worker behaviour.

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : UnitBase
{
    private void Awake() {
        _baseAwake();
    }

    private void Update() {
        _baseUpdate();
    }

}
