﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
public class Grid : Singleton<Grid>
{
    public class Cell
    {
        public bool empty;
        public Vector3 center;
        public int x, y;
        public Cell(bool empty, Vector3 center, int x, int y)
        {
            this.empty = empty;
            this.center = center;
            this.x = x;
            this.y = y;
        }

        public override bool Equals(object obj)
        {
            Cell other = (Cell) obj;
            return x == other.x && y == other.y;
        }
    }


    [SerializeField]
    private float cellSize;
    [SerializeField]
    private int width;
    [SerializeField]
    private int height;

    [SerializeField]
    private bool showGrid = false;

    public static List<List<Cell>> grid;

    private void Awake() {
        CreateLineMaterial();
        // Make width and height even numbers
        if (width % 2 == 1) width++;
        if (height % 2 == 1) height++;
        
        Initialize();
    }

    private void Initialize()
    {
        grid = new List<List<Cell>>();
        for (int i=0; i<width; i++)
        {
            grid.Add(new List<Cell>());
            for (int j=0; j<height; j++)
            {
                grid[i].Add(new Cell(true, CellCenter(i, j), i, j));
            }
        }

    }

    public Cell CellAt(int i, int j)
    {
        if (i < 0 || j < 0 || i >= grid.Count || j >= grid[0].Count)
            return null;
        return grid[i][j];
    }
    public Cell CellAtPoint(float x, float y)
    {
        int col = Mathf.FloorToInt(x+width/2); // Since the center of the grid should be at 0,0
        int row = Mathf.FloorToInt(y+height/2);
        return grid[col][row];
    }

    public Vector3 CellLowerLeft(int i, int j)
    {
        return new Vector3(i*cellSize-width/2, j*cellSize-height/2, 0f);
    }

    public Vector3 CellCenter(int i, int j)
    {
        return new Vector3(i*cellSize + cellSize/2 - width/2, j*cellSize + cellSize/2 - height/2, 0f);
    }


    // DRAWING
    
     // When added to an object, draws colored rays from the
    // transform position.
    public int lineCount = 100;
    public float radius = 3.0f;

    static Material lineMaterial;
    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    // Will be called after all regular rendering is done
    public void OnRenderObject()
    {
        if (showGrid)
            DrawGrid();
    }


    private void DrawLine(Vector3 from, Vector3 to)
    {
        GL.Vertex3(from.x, from.y, from.z);
        GL.Vertex3(to.x, to.y, to.z);
    }
    public void DrawGrid() {
        // Apply the line material
        lineMaterial.SetPass(0);

        GL.PushMatrix();

        // Draw Lines
        GL.Begin(GL.LINES);
        
        Vector3 from, to;
        GL.Color(new Color(0, 0, 0, 0.25f));
        // Horizontal
        from = new Vector3(-width/2, height/2, 0);
        to = new Vector3(width/2, height/2, 0);
        while (from.y >= -height/2)
        {
            DrawLine(from, to);

            from.y -= cellSize;
            to.y -= cellSize;
        }

        // Vertical
        from = new Vector3(width/2, -height/2, 0);
        to = new Vector3(width/2, height/2, 0);
        while (from.x >= -width/2)
        {
            DrawLine(from, to);

            from.x -= cellSize;
            to.x -= cellSize;
        }
        
        GL.End();
        GL.PopMatrix();


        // Apply the line material
        lineMaterial.SetPass(0);

        GL.PushMatrix();

        // Draw Quads
        GL.Begin(GL.QUADS);
        Vector3 cellCoord;
        for (int i=0; i<grid.Count; i++)
        {
            for (int j=0; j<grid[i].Count; j++)
            {
                // Get southwest point of cell
                cellCoord = CellLowerLeft(i, j);

                // Determine color
                if (grid[i][j].empty)
                {
                    GL.Color(new Color(0, 1, 0, 0.25f));
                }
                else
                {
                    GL.Color(new Color(1, 0, 0, 0.25f));
                }

                // Add all quad vertices: southwest, southeast, northeast, northwest
                GL.Vertex3(cellCoord.x, cellCoord.y, cellCoord.z);
                GL.Vertex3(cellCoord.x, cellCoord.y+cellSize, cellCoord.z);
                GL.Vertex3(cellCoord.x+cellSize, cellCoord.y+cellSize, cellCoord.z);
                GL.Vertex3(cellCoord.x+cellSize, cellCoord.y, cellCoord.z);
            }
        }
        
        GL.End();
        GL.PopMatrix();


    }

}
